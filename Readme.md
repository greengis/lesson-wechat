## 微信小程序设计与基础开发课程

#### 课程目标
01. 学习微信小程序的搭建流程
02. 微信小程序页面设计
03. 微信小程序地图与导航
04. 微信小程序调用三方组件
05. 微信小程序Http数据请求
06. 微信小程序拍照上传
07. 微信小程序二维扫码

#### 目标界面
![输入图片说明](images/1.jpg)

#### 准备课程
00. Git命令以及Gitee代码仓库（基础）（主课程01之前）（参见lesson-gitee）
01. HTML基础（主课程02之前）（参见lesson-html lesson-01）
02. CSS基础（主课程02之前）（参见lesson-html lesson-02）
03. JS基础（主课程03之前）（参见lesson-javascript lesson-01&02&03&04）

#### 课程内容
01. 首个小程序及微信小程序示例及如何调试（基础）(分支lesson-01)
02. 小程序页面设计（基础）(分支lesson-02)
03. 小程序地图导航（基础）(分支lesson-03)
04. 小程序地图及要素加载（基础）(分支lesson-04)
05. 实时监测及Chart（基础）(分支lesson-05)
06. 小程序Http请求及数据API（基础）(分支lesson-06)
07. 小程序绑定平台用户的前端实现（进阶）(分支lesson-07)
08. 小程序工单流程LIST-DETAIL（进阶）(分支lesson-08)
09. 小程序拍照及上传照片（进阶）(分支lesson-09)
10. 小程序扫码及二维码生成（进阶）(分支lesson-10)

#### 课外进阶
01. MongoDB非关系数据库（基础）（主课程07之前）（参见lesson-express lesson-01）
02. Nodejs+Express后端Restful API设计（进阶）（主课程07之前）（参见lesson-express lesson-02&03）
03. 小程序绑定平台用户的后端实现（进阶）（主课程07之前）（参见lesson-express lesson-06）


> PS: 注意获取对应分支，准备课程以及课外进阶可以参考其它相关课程<br/><br/>
> 分支clone命令(他人仓库，只读，请clone) <br/>
> git clone -b lesson-01 https://gitee.com/greengis/lesson-wechat.git "lesson-01" <br/>
> <br/>
> 分支获取流程(自己或合作者仓库，读写，请pull+checkout)<br/>
> 01. git init
> 02. git remote add origin https://gitee.com/greengis/lesson-wechat.git
> 03. 获取master: git pull origin master
> 04. 获取指定分支: git pull --all
> 05. 获取指定分支: git checkout -b lesson-01 origin/lesson-01


#### 实例效果
![输入图片说明](images/1.jpg)
![输入图片说明](images/2.jpg)
![输入图片说明](images/3.jpg)
![输入图片说明](images/4.jpg)
![输入图片说明](images/5.jpg)
![输入图片说明](images/6.jpg)


